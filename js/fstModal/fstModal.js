/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 Fst BOX function
 */
window.debug = true; 
var FstModalClass = this.FstModal = new Class({
	Implements:[Options,Events],
	options: {
		'enable_header':true, // povoleni hlavicka
		'enable_footer':true, // povoleni paticka
		'enable_close':true, // povoleni zaviraci krizek
		'content':'', // content v JSON cele HTML misto request, pokud je prazdna bere request, result request musi byt die(json_encode(array('r'=>true,'html'=>'<p>test</p>')));
		'modal_id':'myModal', // ID modalniho okna
		'class_el':'fst_modal', // class na odkaz
		'own_class':'fst_modal_class', // class na modalni okno pro prestylovani
		'onComplete':null, // class na modalni okno pro prestylovani
		'vertical_position':'center ', // prazdna hodnota je pozice nahoru, hodnota "center" je na stred
		'modal_class':{ // default modal class
			'content':'modal-content-default',
			'header':'modal-header',
			'body':'modal-body',
			'footer':'modal-footer',
			'preloader':'modal-preloader',
		}, 
		
		
	},
	// init fce
	initialize:function(options){
		if (window.debug) console.log('run fst box');
		this.setOptions(options);
		
		this.init_events();
	},
	
	// init events by class
	init_events: function(){
		
		$$('.'+this.options.class_el).addEvent('click',this.click_event.bind(this));
	},
	
	// click event
	click_event: function(e){
		e.stop();
		this.link = e.target;
		
		this.getParams();
		this.createModal();
		
		JQ_open_modal(this.options.modal_id);
		
		this.loadContent();
	},
	
	// get params from data-params
	getParams: function(){
		if (this.link.get('data-params')){
			options = JSON.decode(this.link.get('data-params'));
			this.setOptions(options);
			
			//console.log(this.options);
			//console.log(this.params);
		}
		
		if (this.link.get('title')){
			this.modal_title = this.link.get('title');
		}
	},
	
	// createModal
	createModal: function(){
		//this.modal_over = new Element('div',{'class':'modal-backdrop fade in'}).inject($('body'));
		this.modal = new Element('div',{'class':this.options.vertical_position+' modal fade in','role':'dialog','id':this.options.modal_id}).inject($('body'));
		this.modal_dialog = new Element('div',{'class':'modal-dialog '+this.options.own_class}).inject(this.modal);
		this.modal_content = new Element('div',{'class':this.options.modal_class.content}).inject(this.modal_dialog);
		this.modal_preloader = new Element('div',{'class':this.options.modal_class.preloader}).inject(this.modal_content);
		
		if (this.options.enable_header){
			this.modal_header = new Element('div',{'class':this.options.modal_class.header}).inject(this.modal_content);
			if (this.options.enable_close){
				this.modal_close = new Element('div',{'class':'close fa fa-close','data-dismiss':'modal'}).inject(this.modal_header);
			}
			
			if (this.modal_title){
				this.modal_close = new Element('h4',{'class':'modal-title'}).set('text',this.modal_title).inject(this.modal_header);
			}
		
		}
		this.modal_body = new Element('div',{'class':this.options.modal_class.body}).inject(this.modal_content);
		
		if (this.options.enable_footer){
			this.modal_footer = new Element('div',{'class':this.options.modal_class.footer}).inject(this.modal_content);
		}
		
		
	},
	
	
	// preloader on/off
	modalPreloader: function(){
		if (this.modal_preloader.hasClass('none')){
			this.modal_preloader.removeClass('none');
		} else {
			this.modal_preloader.addClass('none');
		}
	},
	
	// load content
	loadContent: function(){
		
		// pokud ma data-content
		if (this.options.content && this.options.content != ''){
			this.content = this.options.content;
			this.renderContent();
			
		// pokud neni data-content zavolej request
		} else {
			this.loadRequest();
		}
	},
	
	// render content
	renderContent: function(){
		if (this.content){
			this.modal_body.set('html',this.content);
			this.modalPreloader();
			this.initAfterRender();
		}
	},
	
	// init after render
	initAfterRender: function(){
		if (this.options.onComplete){
			eval(this.options.onComplete);
		}
	},
	
	// load request
	loadRequest: function(){
		this.req_load = new Request.JSON({
			url:this.link.href,
			onError: this.req_error = (function(data){
				if (window.debug) console.log('chyba nacteni modal');
			}).bind(this),
			onComplete :(function(json){
				if (json.r == true){
					if (json.html){
						this.content = json.html;
						this.renderContent();
					} else {
						console.log('v JSON neni pole HTML');
					}
				}
			}).bind(this)
		});
		this.req_load.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_load.send();
	},
	
	

});
window.addEvent('domready',function(){
	var FstModal = new FstModalClass();
	//console.log(this.Fst.test());
});
	




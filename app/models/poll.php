<?php
class Poll extends AppModel {
    var $name = 'Poll';
	
	
	function beforeSave(){
		if (isset($this->data[$this->name]['data']))
			$this->data[$this->name]['data'] = serialize($this->data[$this->name]['data']);
		
		
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				
				if (isset($data[$key][$this->name]['data'])){
					$data[$key][$this->name]['data'] = unserialize($data[$key][$this->name]['data']);
				}
				
			
			}
		}
		return $data;
    }
}
?>
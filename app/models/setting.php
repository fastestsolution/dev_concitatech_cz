<?php
class  Setting extends AppModel {
    var $name = 'Setting';
	//var $actsAs = array('Trans'=>array('cols'=>array('value')));
	
	function beforeSave(){
		
		if (isset($this->data[$this->name]['value']))
			$this->data[$this->name]['value'] = serialize($this->data[$this->name]['value']);
		
		return $this->data;
	}
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($item[$this->name]['value'])){
					$data[$key][$this->name]['value'] = unserialize($item[$this->name]['value']);
				}
			}
		}
		//pr($data);
		
		return $data;
    }
	
}
?>
<?php
class Translation extends AppModel {
   var $name = 'Translation';
   
	function beforeSave(){
		if (isset($this->data[$this->name]['col']) && $this->data[$this->name]['col'] == 'name'){
			$this->saveAlias($this->data);
		}
		return $this->data;
	}
	
	function afterSave($success){
		if ($success){
			if (isset($this->aliasForSave)){
				$tmp = $this->aliasForSave;
				unset($this->aliasForSave);
				$this->id = null;
				$sql = 'UPDATE fastest__translations SET history=1 WHERE parent_id='.$tmp[$this->name]['parent_id'].' AND modul="'.$tmp[$this->name]['modul'].'" AND col="alias_" AND lang="'.$tmp[$this->name]['lang'].'"';
				$this->query($sql);
				$this->save($tmp);
			}
		}
	}
   
	function saveAlias($data){
		$this->aliasForSave = array($this->name => array(
			'lang' 		=> $data[$this->name]['lang'],
			'col'		=> 'alias_',
			'value'		=> $this->createAlias($data[$this->name]['value']),
			'created'	=> $data[$this->name]['created'],
			'modul'		=> $data[$this->name]['modul'],
			'parent_id'	=> $data[$this->name]['parent_id']
		));
	}
   
	function afterFind($data){
		$output = array();
		if (isset($data) && count($data) > 0)
			foreach ($data as $k => $items){
				if (is_array($items) && array_key_exists('Translation',$items)){
					foreach($items['Translation'] as $key => $item){
						if (is_array($item) && array_key_exists('col',$item)){
							$output[$k]['Translation'][$item['lang']][$item['modul']][$item['col']] = $item['value'];
						}
					}
				}
			}		
		return $output;
    }
}
?>
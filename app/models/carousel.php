<?php
class  Carousel extends AppModel {
    var $name = 'Carousel';
	//var $actsAs = array('Trans'=>array('cols'=>array('title')));
	
	var $cacheSources = false;
	 
	function beforeSave(){
		if (isset($this->data[$this->name]['items'])){
			$this->data[$this->name]['items'] = json_encode($this->data[$this->name]['items']);
                }
		return $this->data;
        }
	
	function afterFind($data){
		
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($item[$this->name]['items'])){
					$data[$key][$this->name]['items'] = json_decode($data[$key][$this->name]['items'], true);
                                        if(!$data[$key][$this->name]['items']){
                                            $data[$key][$this->name]['items'] = array();
                                        }
                                }
			}
		}
		return $data;
    }	
}
?>
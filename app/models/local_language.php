<?php
class  LocalLanguage extends AppModel {
    var $name = 'LocalLanguage';
	var $basket_translation = 'LocalLanguageName';
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($this->basket_translation) && isset($item[$this->basket_translation])){
					$data[$key][$this->name]['name'] = $item[$this->basket_translation]['value'];		
				}
			}
		}
		return $data;
    }
}
?>
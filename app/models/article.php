<?php
class Article extends AppModel {
    var $name = 'Article';
   // var $actsAs = array('Trans'=>array('cols'=>array('name','alias_','text','keywords','description')));
	
		
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				
				if (isset($data[$key][$this->name]['addons'])){
					$data[$key][$this->name]['addons'] = unserialize($data[$key][$this->name]['addons']);
				}
				if (isset($data[$key][$this->name]['fotogaleries'])){
					$data[$key][$this->name]['fotogaleries'] = unserialize($data[$key][$this->name]['fotogaleries']);
				}
				if (isset($data[$key][$this->name]['text'])){
					$data[$key][$this->name]['text'] = $this->text_replace($data[$key][$this->name]['text']);
				}
			
			}
		}
		return $data;
    }
	
	function text_replace($text){		
		//$text = tidy_repair_string($text,array('show-body-only' => true,'doctype' => '-//W3C//DTD XHTML 1.0 Transitional//EN','output-xhtml' => true),'utf8');
		
		/*
		preg_match_all('/##Fotogalerie.(.*)##/ismU', $text, $fotogalerie);
		//pr($fotogalerie);
		if (isset($fotogalerie) && (isset($fotogalerie[0][0]))){
			foreach($fotogalerie[0] AS $foto){
				//pr($foto);
				list($model,$foto_id) = explode('.',$foto);
				$url = '/clanek-fotogalerie/'.strtr($foto_id,array('##'=>''));
				
				$galery = $this->requestAction($url, array ('return'));
				//pr($galery);
				$text = strtr($text,array($foto=>$galery));
				
			}
		}
		*/
		
		preg_match_all('/<img (.*) \/>/ismU', $text, $youtube);
		
		foreach($youtube[0] AS $img){

			$doc = new DOMDocument();
			$doc->strictErrorChecking = FALSE;
			$doc->loadHTML('<?xml encoding="UTF-8">' .$img);
			$xml = simplexml_import_dom($doc);
			$im = get_object_vars($xml->body->img);
			
			if (isset($im['@attributes']['class']) && ($im['@attributes']['class'] == ' nicEdit-youtube' || $im['@attributes']['class'] == 'nicEdit-youtube')){
				$replace = '<iframe class="noprint youtube" width="560" height="349" src="http://www.youtube.com/embed/'.$im['@attributes']['alt'].'?theme=light&amp;color=#4CC5C8" frameborder="0" ></iframe>';
				$text = strtr($text,array($img=>$replace));
				
			}
			if (isset($im['@attributes']['class']) && ($im['@attributes']['class'] == ' nicEdit-fotogalerie' || $im['@attributes']['class'] == 'nicEdit-fotogalerie')){
				list($model,$foto_id) = explode('.',$im['@attributes']['alt']);
				$url = '/clanek-fotogalerie/'.strtr($foto_id,array('##'=>''));
				
				$galery = $this->requestAction($url, array ('return'));
				
				$text = strtr($text,array($img=>$galery));
				
			}
			if (isset($im['@attributes']['data-zoom']) && ($im['@attributes']['data-zoom'] == true)){
				//pr($im);
				//$link = '<a href="'.$im['@attributes']['src'].'" rel="lightbox" title="'.$im['@attributes']['alt'].'">'.$img.'</a>';
				$link = '<a href="'.$im['@attributes']['src'].'" rel="lightbox" title="'.$im['@attributes']['alt'].'"><img src="/image_resize.php?file=.'.$im['@attributes']['src'].'&amp;w='.$im['@attributes']['width'].'&amp;h='.$im['@attributes']['height'].'" alt="'.$im['@attributes']['alt'].'" class="'.(($im['@attributes']['class'])?$im['@attributes']['class']:'').'" /></a>';
				//pr($link);
				$text = strtr($text,array($img=>$link));
			
			}
		}
		return $text;
		
	}
}
?>
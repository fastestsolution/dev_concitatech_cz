<?php
define ('DEFAULT_LANGUAGE','cz');
define ('ACTIVE_LANGUAGE',true);

if (isset($_GET['lang']))
	$lang = $_GET['lang'];
else $lang = DEFAULT_LANGUAGE;
define ('CURRENT_LANGUAGE',$lang);

class AppController extends Controller {
	var $components  = array('RequestHandler','Pagination');
	var $helpers = array('htmlExt','Pagination','Fastest');
	
	
	/*
	* BEFORE FILTER
	*/
	function beforeFilter(){
		if (isset($_GET['lang']))
			$lang = $_GET['lang'];
		else 
			$lang = DEFAULT_LANGUAGE;
		
		@define ('CURRENT_LANGUAGE',$lang);

		$lang=CURRENT_LANGUAGE==DEFAULT_LANGUAGE ? "/" : "/".CURRENT_LANGUAGE;
		$this->set('lang',$lang);
		
		if ($this->params['url']['url'] == '/')	$this->set('on_main_page',true);
			
		include "select_config.php";
		self::load_page_setting();
					
		self::load_language_setting();
		self::load_menu_item();
		
		
		unset($this->Setting->behaviors['Trans']);

		$this->set('brand_title','Concia tech');
	}
	/*
	* BEFORE RENDER
	*/
	function beforeRender(){
		self::gen_keywords(isset($this->global_keywords_data)?$this->global_keywords_data:null,(isset($this->keywords_data)?$this->keywords_data:null));
		self::check_browser();
		self::smallboxs();
		self::load_js();
		self::checkIE();
		
		// render css file from LESS
		if (!$this->RequestHandler->isAjax() && $this->params['url']['url'] == '/'){
			if (!Configure::read('compress')){
				$this->renderCss();
			}
		}
	}
	
	function checkIE(){ 
		preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
		if(count($matches)<2){
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
		}

		if (count($matches)>1){
		//Then we're using IE
		$version = $matches[1];
		// pr($version);die();
		switch(true){
			case ($version<=10):
				$this->redirect('http://browsers.fastest.cz');
			//IE 8 or under!
			break;

			case ($version==9 || $version==10):
			//IE9 & IE10!
			break;

			case ($version==11):
			//Version 11!
			break;

			default:
			//You get the idea
		}
		}
	}
	
	 private function renderCss(){
    	// run compile LESS to CSS
		$ch = curl_init();
		
		//pr($_SERVER);
		//$link = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/compile.php?render";
		$link = $_SERVER['HTTP_HOST']."/compile.php?render";
		curl_setopt($ch, CURLOPT_URL, $link);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$res = curl_exec($ch);

		curl_close($ch);
		
	}
	
	function check_browser(){
		if (stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') || stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') ) {
			$this->set('check_browser',true);
		}
	}
	/*
	* LOAD JS AND CSS
	*/
	private function load_js(){
        require_once('min/lib/Minify.php');
        require_once('min/lib/Minify/Source.php');
        require_once('min/utils.php');
		$debug_list_load = include('./min/groupsConfig.php');
		if (Configure::read('compress')){
			$this->set('compress',true);
			
			$jsUri = Minify_getUri('js'); // a key in groupsConfig.php
			$this->set('jsUri',$jsUri.JS_VERSION);		
			$cssUri = Minify_getUri('css'); // a key in groupsConfig.php
			$this->set('cssUri',$cssUri.CSS_VERSION);
			
			
			
			//pr($cssUri);
		
			
			//Cache::disable();
			Configure::write('Cache.disable', false);
		} else {
			Configure::write('Cache.disable', true);
			//Cache::enable();
			$debug_css = array(
				'css'=>$debug_list_load['css_links'],
				'js'=>$debug_list_load['js_links'],
			);
			$this->set('debug_css',$debug_css);
			
		}
		//pr(Configure::read('compress_html'));die();
		//unset(debug_list_load)
				
    }
	/*
	* SMALLBOX
	*/
	function smallboxs(){
		$this->loadModel('Smallbox');
		
		//if (($smallbox_list = Cache::read('smallbox_list')) === false) {
			$smallbox_list = $this->Smallbox->find('all',array('conditions'=>array('Smallbox.status'=>1,'Smallbox.kos'=>0),'fields'=>array('name','text','id','title')));
		
			//Cache::write('smallbox_list', $smallbox_list);
		//}
		//pr($smallbox_list);
		$smallbox_list = Set::combine($smallbox_list, '{n}.Smallbox.id', '{n}');
		$this->set('smallbox_list',$smallbox_list);
	}
	/*
	* LOAD MENU ITEM
	*/
	function load_menu_item(){
			$this->loadModel('MenuItem');
			
			
			//$menu_tabule = $this->MenuItem->read(null,1);
			//$menu_vertikalni = $this->MenuItem->read(null,13);
			$conditions = array(
				'MenuItem.status'=>1,
				'MenuItem.name !=' => ''
			);
			
			//$conditions_horni = am($conditions,array('MenuItem.lft > '=>$menu_horni['MenuItem']['lft'],'MenuItem.rght < '=>$menu_horni['MenuItem']['rght']));
			/*
			$conditions_tabule = am($conditions,array('MenuItem.lft >'=>$menu_tabule['MenuItem']['lft'],'MenuItem.rght < '=>$menu_tabule['MenuItem']['rght']));
			$conditions_vertilakni = am($conditions,array('MenuItem.lft >'=>$menu_vertikalni['MenuItem']['lft'],'MenuItem.rght < '=>$menu_vertikalni['MenuItem']['rght']));
			*/
			$fields= array(
			
				'MenuItem.id',
				'MenuItem.parent_id',
				'MenuItem.name',
				'MenuItem.title',
				'MenuItem.alias_',
				'MenuItem.spec_url',	
				'MenuItem.show_menu',	
			
			);
			
			if (($menu_item = Cache::read('menu_item')) === false) {
				$menu_item = $this->MenuItem->find('threaded',array('fields'=>$fields,'conditions'=>$conditions,'order'=>'lft ASC'));
				Cache::write('menu_item', $menu_item);
			}
			
 	       	$this->menu_item = $menu_item;
 	       	//pr($this->menu_item);
			/*
			$this->menu_item2 = $this->MenuItem->find('threaded',array('fields'=>$fields,'conditions'=>$conditions_horni,'order'=>'lft ASC'));
 	       	$this->menu_item3 = $this->MenuItem->find('threaded',array('fields'=>$fields,'conditions'=>$conditions_vertilakni,'order'=>'lft ASC'));
		    */
			//pr($this->menu_item3);
			$this->menu_item_sitemap = $this->menu_item;
			//unset($this->menu_item_sitemap[7]);
			//pr($menu_item_sitemap);
			$this->set('menu_item_sitemap',$this->menu_item_sitemap);
			$this->set('menu_item',$this->menu_item);
            //$this->set('menu_item2',$this->menu_item2);
			//unset($this->menu_item3[7]);
            //$this->set('menu_item3',$this->menu_item3);
			
	}
	
	/*
	* LOAD PAGE SETTING  
	*/
	function load_page_setting(){
			if (!isset($this->Setting)) $this->loadModel('Setting');
			if (($settings = Cache::read('settings')) === false) {
				$settings = $this->Setting->find('all',array('fields'=>array('Setting.id','Setting.value')));
				Cache::write('settings', $settings);
			}
			foreach($settings as $set){
				// pr($set); die();
				if($set['Setting']['id'] == 1) $this->set('setting',$this->setting = $set['Setting']['value']); 
			}
			$this->global_keywords_data = array(
				'global_keywords'=>$this->setting['keywords'],
			); 
	}
	
	/*
	* LOAD LANGUAGE SETTING
	*/
	function load_language_setting(){
			//pr('a');
			$this->loadModel('LocalLanguage');
			if (($lang = Cache::read('lang')) === false) {
				$lang = $this->LocalLanguage->find('all',array('fields'=>array('name',CURRENT_LANGUAGE)));
			
				Cache::write('lang', $lang);
			}
			
			
			unset($this->LocalLanguage);
			foreach ($lang as $item){
				if (!defined ('lang_'.$item['LocalLanguage']['name'])) 
					define('lang_'.$item['LocalLanguage']['name'],$item['LocalLanguage'][CURRENT_LANGUAGE]);
				if (!isset($controller->lng[$item['LocalLanguage']['name']])){
					$controller->lng[$item['LocalLanguage']['name']] = $item['LocalLanguage'][CURRENT_LANGUAGE];
				}
			}
			if (isset($this->params['url']['url']))
			$this->set('actual_url',$this->actual_url = explode('/',$this->params['url']['url']));
			
	}
	
	
	/*
	* GENEROVANI META TAGU
	*/
	function gen_keywords($global_keywords_data=null,$keywords_data=null){
		//pr($global_keywords_data);
		//pr($keywords_data);
		
		// klicova slova
		if (isset($keywords_data['keywords']) && !empty($keywords_data['keywords'])){
			$keywords = $keywords_data['keywords'];
		} else {
			$keywords = $global_keywords_data['global_keywords'];		
		}
		
		// description
		if (isset($keywords_data['description']) && !empty($keywords_data['description'])){
			$description = $keywords_data['description'];
		} elseif(isset($keywords_data['text']) && !empty($keywords_data['text'])){
			$description = $keywords_data['text'];
		} else {
			$description = $global_keywords_data['global_keywords'];		
		}		
		
		$this->set('keywords',$keywords);
		$this->set('description',$description);
	}
	
	
	/*** ENCODE LONG URL **/	
	function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
    /*** DECODE LONG URL **/	
	function decode_long_url($data){
    	$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
    }
	
	
	/*
	* ERROR PAGE 404
	*/
	function error_404(){
		header("HTTP/1.0 404 Not Found");
		if (!isset($this->Error404)){ $this->loadModel('Error404');}
			$this->data['Error404']['browser']=$_SERVER['HTTP_USER_AGENT'];
			$this->data['Error404']['name']=$_SERVER['REMOTE_ADDR'];
			$this->data['Error404']['to']= $_SERVER['REQUEST_URI'];
			
			if (isset($_SERVER['HTTP_REFERER'])) $this->data['Error404']['from']=$_SERVER['HTTP_REFERER'];
			$this->Error404->save($this->data);
		unset($this->Error404);
		
		$this->set('page_caption',lang_error_404_title);
		$this->set('fastlinks',array(lang_error_404_title => '#'));
		echo $this->render('../articles/404');
		die();	
	}
}
?>
<p class="copyright">&copy; 2019 Concia tech s.r.o., Všechna práva vyhrazena, Vytvořilo <a href="http://www.fastest.cz/" target="_blank"  title="Tvorba webových stránek" onclick="_gaq.push(['_trackEvent', 'Odkazy','Kliknuti', 'Fastest Solution']);">Fastest Solution s.r.o.</a></p>
<a class="fb-link" href="https://www.facebook.com/pribylar/">
    <svg class="fb-icon" enable-background="new 0 0 486.392 486.392" version="1.1" viewBox="0 0 486.392 486.392" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                <path d="m243.2 0c-134.3 0-243.2 108.89-243.2 243.2s108.89 243.2 243.2 243.2 243.2-108.89 243.2-243.2c0-134.34-108.89-243.2-243.2-243.2zm62.866 243.16l-39.854 0.03-0.03 145.92h-54.689v-145.92h-36.479v-50.281l36.479-0.03-0.061-29.609c0-41.039 11.126-65.997 59.431-65.997h40.249v50.311h-25.171c-18.817 0-19.729 7.022-19.729 20.124l-0.061 25.171h45.234l-5.319 50.28z" />
    </svg>
</a>

<script type="text/javascript" src="/js/aos/aos.js"></script>
<script type="text/javascript">
    AOS.init({
        easing: 'ease-in-out-sine'
    });
</script>
 
<div class="wrapper">
	<div class="mobileTarifs internet">

		<div class="mobileT">
			<div class="title">LTE<br />Rapid</div>
			
            <div class="row">
                <label>Propojte se s námi, my dobře víme, co potřebujete. Spolehlivý a rychlý internet.</label>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>20/3<br/><span>Mbps</span></div>

		</div>
        <div class="mobileT">
            <div class="title">LTE Rapid<br />s prémií</div>

            <div class="row">
                <label>Naše skvělá nabídka – spolehlivé internetové připojení, televize a k tomu PODA net.TV.</label>
                <a href="/tv-sluzby/seznam-programu/">Základní tv nabídka</a>
                <br>
                <a href="/tv-sluzby/podanet-tv/">1 PODA net. TV zdarma</a>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>20/3<br/><span>Mbps</span></div>

        </div>
        <div class="mobileT">
            <div class="title">LTE Rapid<br />rychlý</div>

            <div class="row">
                <label>Vysokorychlostní internet, díky kterému budete vždycky vědět víc. Propojíme i vaši rodinu.</label>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>30/5<br/><span>Mbps</span></div>

        </div>
        <div class="mobileT">
            <div class="title">LTE Rapid<br />naplno</div>

            <div class="row">
                <label>PODA internet naplno – vysoká rychlost stahování a rozšířená nabídka televizních programů k tomu.</label>
                <a href="/tv-sluzby/seznam-programu/">Rozšířená TV nabídka</a>
                <br>
                <a href="/tv-sluzby/podanet-tv/">2 PODA net. TV zdarma</a>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>30/5<br/><span>Mbps</span></div>

        </div>
	</div>
	
	<br class="clear" />
</div>


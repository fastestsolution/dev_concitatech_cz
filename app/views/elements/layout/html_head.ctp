<?php #<!-- META --> ?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name='description' content='<?php echo @$page_caption.' '.$description ?>'/>
<meta name='keywords' content='<?php echo $keywords ?>'/>
<meta name="robots" content="all,index,follow" />
<meta name='author' content='Fastest Solution, 2019 All Rights Reserved url: http://www.fastest.cz' />
<meta name='dcterms.rights' content='Fastest Solution, 2019 All Rights Reserved url: http://www.fastest.cz' />

<?php #<!-- RESPONSIVE SETTING -->	?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<?php #<!-- FB --> ?>

<meta property="og:title" content="<?php echo (isset($og_graph['title'])?$og_graph['title']:@$page_caption)?>" />
<meta property="og:description" content="<?php echo (isset($og_graph['description'])?$og_graph['description']:$description)?>" />
<?php #<meta property="og:image" content="<?php echo (isset($og_graph['img'])?$og_graph['img']:'http://'.$_SERVER['HTTP_HOST'].'/css/fastest/layout/logo.png')" />?>

<?php if (!empty($setting['ga_superadmin_valid'])): ?>
    <meta name="google-site-verification" content="<?php echo $setting['ga_superadmin_valid'] ?>" />
<?php endif; ?>

<?php #<!-- LINKS --> ?>
<link href='/css/layout/icons/favicon.png' rel='icon' />

<?php #<!-- FAVICON --> ?>
<link rel="apple-touch-icon" sizes="57x57" href="/css/layout/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/css/layout/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/css/layout/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/css/layout/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/css/layout/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/css/layout/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/css/layout/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/css/layout/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/css/layout/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/css/layout/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/css/layout/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/css/layout/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/css/layout/favicon/favicon-16x16.png">
<link rel="manifest" href="/css/layout/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/css/layout/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<title><?php echo $page_caption;?> <?= ($brand_title ? ' - '. $brand_title: ''); ?></title>



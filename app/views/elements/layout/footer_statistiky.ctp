<div class="noprint">
<?php 
$domena = $_SERVER['HTTP_HOST'];
//$pos = strpos($domena, 'www');
//if($pos !== false) $domena = substr($domena,4);


?>
<?php if (!empty($setting['ga'])){ ?>

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $setting['ga'] ?>', '<?php echo $domena ?>');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
<?php } ?>
<?php if (!empty($setting['ga_superadmin'])){ ?>
<script type="text/javascript">
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $setting['ga_superadmin'] ?>', '<?php echo $domena ?>');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
<?php } ?>
</div>

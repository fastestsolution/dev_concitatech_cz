<h2 class="txt-center uppercase mt50">Často kladené otázky</h2>
		
<div id="faq" class="faq mt50">
	<div class="mainTitle">Řešení potíží s obrazem a zvukem</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Vůbec mi nehraje televize, co mám dělat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Nehraje mi některý z programů, ostatní jsou v pořádku, kde je chyba?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Co mám dělat, když obrazovka je černá a v rohu se střídají symboly HDMI1, HDMI2, Tv, AV?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Na obrazovce vidím napsáno “Není signál”</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Obraz je širší (užší) než obrazovka. Co s tím?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Proč mi nejde zvuk?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Obraz není kvalitní - trhá se, vznikají čtverečky. Co mám dělat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Zvuk není synchronizovaný s obrazem. Co mám dělat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="mainTitle mt50">TV Kanály</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Můžu měnit pořadí TV kanálů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Může mít každý člen rodiny vlastní řazení kanálů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Jak zobrazím informace o právě sledovaném pořadu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	
	<div class="mainTitle mt50">Chytré funkce - Zpětné shlédnutí</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Jak funguje zpětné shlédnutí?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Na jak dlouho můžu nastavit živé vysílání televize?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Můžu používat funkci zpětné shlédnutí u kteréhokoliv kanálu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Zpět zpět zhlédnutí stojí?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	
	<div class="mainTitle mt50">Chytré funkce - Nahrávání</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Co potřebuji, abych mohl začít využívat nahrávání?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Můžu nahrávat pořad, na který se právě dívám?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Jak si můžu nahrávání nastavit?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Jaká je kapacita nahrávání?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Můžu si nastavit opakované nahrávání, třeba u seriálů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Můžu nahrávat pořady, které už byly odvysílané?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Co když nahrávaný program skončí později, než je uvedeno v programu? Nepřijdu o jeho konec?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Můžu nahrávat všechny pořady ze všech TV kanálů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Můžu během nahrávání sledovat jiný TV kanál?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>

	<div class="mainTitle mt50">Chytré funkce - Archiv</div>
	
	<div class="fRow bottomLine mt25">
		<div class="fRowTitle">Kde zjistím, které pořady se nahrávají do archivu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="mainTitle mt50">PODA NET.TV</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Obecně o PODAnet.tv</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Platí u PODA net.TV vše jako u Tv vysílání?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
<br class="clear" />

<a href="" class="button blue">Dokumenty ke stažení</a>
<div class="wrapper">
	<div class="mobileTarifs internet">

		<div class="mobileT">
			<div class="title">Základní nabídka</div>
			
            <div class="row">
                <label>Krátký popis služby.</label>
                <label>Aktivace internetu jednorázově 1150,-</label>
                <label>PODA net.TV</label>
                <a href="/tv-sluzby/seznam-programu/">80 TV programů</a>
                <br/>
                <a href="#tableMoreTV">Máte doma více televizí?</a>

            </div>


		</div>
        <div class="mobileT">
            <div class="title">TV<br>Start</div>

            <div class="row">
                <label>Televize bez připojení k internetu. K dispozici je domácnostem, připojeným k optické síti.</label>
                <label>Aktivace internetu jednorázově 1150,-</label>
                <label>1 zařízení PODA net.TV</label>
                <a href="/tv-sluzby/seznam-programu/">50+ TV programů</a>
                <br/>
                <a href="/volani/">1 SIM karta PODA mobil</a>
                <br/>
                <a href="/tv-sluzby/chytre-funkce-tv/">Chytré funkce</a>
                <br/>
                <a href="#tableMoreTV">Máte doma více televizí?</a>

            </div>


        </div>


	</div>
    <br class="clear" />
    <br/>
    <h3>Volitelné varianty nastavení optického připojení k internetu</h3>
    <span>Rychlost (příchozí/odchozí)</span>
    <div class="table-responsive fst-table">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Služba</th>
                <th>Připojení dalšího set-top boxu/měsíc</th>
                <th>Aktivace 2. a další televize</th>

            </tr></thead>
            <tr>
                <td>TV Start</td>
                <td>50 Kč</td>
                <td>325 Kč</td>
            </tr>
            <tr>
                <td>Základní nabídka</td>
                <td>50 Kč</td>
                <td>650 Kč</td>
            </tr>
        </table>
    </div>

	<br class="clear" />
    <div class="wrapper">
        <div class="mt50">
            <h2>Poplatky a slevy k televizní službě</h2>
            <div class="mt25">
                <h3>Aktivace TV služby</h3>
                <p>
                    Poplatek za aktivaci TV služby zahrnuje konfiguraci sítě a set-top boxu, zavedení provozních záznamů, inicializaci diskového prostoru pro PVR, odbornou montáž u zákazníka včetně nastavení jeho TV zařízení.
                </p>
            </div>
            <div class="mt25">
                <h3>Reaktivační poplatek</h3>
                <p>
                    Reaktivační poplatek platí pro každé přerušení v užívání služeb a činí 200 Kč. Přerušení služeb je možné na dobu 2-6 měsíců a lze provést jednou ročně. Přerušení služeb nelze provést během trvání smlouvy na dobu určitou, mohou jej využít klienti se smluvním vztahem na dobu neurčitou.
                </p>
            </div>
            <div class="mt25">
                <h3>Poplatek při předčasném ukončení smlouvy</h3>
                <p>
                    V případě, že klient předčasně požádá o ukončení smlouvy na dobu určitou, je účtován poplatek za předčasné ukončení smlouvy ve výši 20 % součtu zbývajících měsíčních paušálů.
                </p>
            </div>
            <div class="mt25">
                <h3>Sleva pro držitele průkazů ZTP/P</h3>
                <p>
                    Slevu 50 % pro držitele průkazů ZTP/P lze uplatnit na balíčky internet+TV, služby připojení k internetu a televizní službu TV Start, s výjimkou tarifu Senior. Nárok na tuto slevu je nutné doložit platným průkazem ZTP/P (kopii lze zaslat poštou, faxem či naskenovanou v příloze e-mailu na info@poda.cz).
                </p>
            </div>
        </div>
        <div class="mt50">
            <h2>Rozšířený nahrávací prostor až na 40 hodin</h2>
            <div class="mt25">
                <p>
                    Nestačí vám 7 dnů, chcete mít k dispozici celou sérii vašeho oblíbeného pořadu na delší dobu? S individuálním nahrávacím prostorem si můžete váš pořad uchovat až po dobu 3 měsíců. Nahrávání pořadů zvládnou v pohodě i děti. Stiskem červeného tlačítka na ovladači přidáte zvolený pořad, film nebo dokument do vlastních nahrávek. Nahrát si můžete nejen právě hrající pořad, ale i programy, které teprve hrát budou stejně jako kterýkoliv z těch, které již hrály a jsou ještě dostupné v rámci archivu.
                    <br/><br/>
                    Jste na dovolené či na služební cestě a nechcete přijít o napínavé rozuzlení či pokračování seriálu? Nahrávky si můžete komfortně nastavit a spravovat přímo z vaší Klientské zóny kdekoliv od počítače, telefonu či tabletu s připojením k internetu.
                </p>
            </div>
        </div>
        <div class="mt50">
            <h2>Firewall - ochrana vašeho počítače</h2>
            <div class="mt25">
                <p>
                    Ochraňte svá data a počítač. Pořiďte si za příplatek 25 Kč měsíčně firewall PODA a využívejte naplno bezpečné připojení k internetu.
                    <br/><br/>
                    Základní velikost individuálního prostoru je v závislosti na vašem tarifu 10 či 20 hodin. Pokud vám tento prostor nestačí, jednoduše si můžete přikoupit dalších 20 hodin formou balíčku 20za20.
                </p>
            </div>
        </div>
    </div>
    <br class="clear" />
</div>


<div class="wrapper">
    <div class="sll3">
        <a class="swipebox" href="/css/layout/photo/photogallery1.jpg" title="Conciatech">
            <img data-aos="zoom-in" data-aos-offset="500" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery1.jpg" />
        </a>
        <a class="swipebox" href="/css/layout/photo/photogallery2.jpg" title="Conciatech">
            <img  data-aos="zoom-in" data-aos-offset="600" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery2.jpg" />
        </a>
    </div>

    <div class="sll3">
        <a class="swipebox" href="/css/layout/photo/photogallery3.jpg" title="Conciatech">
            <img  data-aos="zoom-in" data-aos-offset="650" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery3.jpg" />
        </a>
        <a class="swipebox" href="/css/layout/photo/photogallery4.jpg" title="Conciatech">
            <img  data-aos="zoom-in" data-aos-offset="500" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery4.jpg" />
        </a>
        <a class="swipebox" href="/css/layout/photo/photogallery5.jpg" title="Conciatech">
            <img  data-aos="zoom-in" data-aos-offset="650" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery5.jpg" />
        </a>
    </div>

    <div class="sll3">
        <a class="swipebox" href="/css/layout/photo/photogallery6.jpg" title="Conciatech">
            <img  data-aos="zoom-in" data-aos-offset="550" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery6.jpg" />
        </a>
        <a class="swipebox" href="/css/layout/photo/photogallery7.jpg" title="Conciatech">
            <img data-aos="zoom-in" data-aos-offset="500" alt="Conciatech image" class="img-responsive photo" src="/css/layout/photo/photogallery7.jpg" />
        </a>
    </div>
</div>

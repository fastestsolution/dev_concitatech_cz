<?php

function list_element($data,$lang, $settings = array(),$level = 0 ){
	$modelName = 'Defualt';
	$fieldName = 'name';
	$ul_id = 'tree_ul_id';
	$prefix = '';	
	extract($settings);
	
	$tabs = "\n" . str_repeat("\t", $level * 2);
	$li_tabs = $tabs . "\t";
  //if($level>0) echo $data[0][$modelName]['name'];
   if($data[0][$modelName]['name']!='') 
	   $output = $tabs. "<ul".(($ul_id != null)?" id='".$ul_id."' class='navbar-nav nav navbar navbar-collapse'":" class='dropdown-menu'").">";
   else $output='';   
	
	
	foreach ($data as $key=>$val){
	 if($val[$modelName]['name']!='' && $val[$modelName]['show_menu'] == 1){
	    $href=($lang!="cz" ? "/".$lang : "");
		$href.=((empty($val[$modelName]['spec_url']))?$prefix.'/'.$val[$modelName]['alias_'].'/':$val[$modelName]['spec_url']);
  		
		$find_target = strpos($href,'http');
		if($find_target !== false){
			$rel = 'nofollow';
			$target = '_blank';
		} else
			unset($target);
		
		$menu_title = (!empty($val[$modelName]['title'])?$val[$modelName]['title']:$val[$modelName]['name']);
		$menu_name = $val[$modelName]['name'];
		
		$str_array = array('&'=>'&amp;');
		$menu_title = strtr($menu_title,$str_array);
		$menu_name = strtr($menu_name,$str_array);
		//pr($actual_url);

		if(!empty($val[$modelName]['imgs'])) {
			$imgs = explode('|', $val[$modelName]['imgs'][0]);
			$img  = '<a href="'.$href.'" class="circle"><img src="/uploaded/menu_item/'.$imgs[0].'" alt="'.$menu_name.'" /></a>';
		} else {
			$img = '';
		}
		
		//.(count($val['children'])>0?' data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>':'')

		$output .= $li_tabs . "<li ".(($actual_url[0] == $val[$modelName]['alias_'] ||  ($_GET['url'] == '/' && $href == '/') ||  ($actual_url[0] == '' && $href == '/'))?
                "class='menu_active sp_".$val[$modelName]['id']." ".(count($val['children'])>0?'':'')."":"class='sp_".$val[$modelName]['id']).' '.(count($val['children'])>0?'':'')." ' >".$img."<a ".(count($val['children']) > 0? '':'')." id='sp".$val[$modelName]['id']."' data-id='".$val[$modelName]['id']."' title='".$menu_title."' class='ajax_href png_fix sp".$val[$modelName]['id']." ".(count($val['children']) > 0?'dropdown-toggle':'')."' href='".$href."' ".(isset($target) && !empty($target)?" target='_blank'":"")." >".$menu_name." ".(count($val['children']) > 0?'<span class="caret"></span>':'')."</a>";
  		if(isset($val['children'][0])){
  			$settings['ul_id'] = null;
  			$settings['actual_url'] = $actual_url;
  			$settings['prefix'] = $prefix.'/'.$val[$modelName]['alias_'];
  			$output .= list_element($val['children'],$lang, $settings, $level+1);
  			$output .= $li_tabs . "</li>";
  		} else {
  			$output .= "</li>";
  		}
		}
	}
	if($data[0][$modelName]['name']!='') 
      $output .= $tabs . "</ul>";
        return $output;
 }

if (isset($menu_item) && !empty($menu_item)){
	echo '
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="containerMenu">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        ';
	echo list_element($menu_item,CURRENT_LANGUAGE , array('modelName'=>'MenuItem','prefix'=>'','subfix'=>'id', 'fieldName'=>'name','ul_id'=>'navmenu','actual_url'=>$actual_url)); 
	echo '</div></div></nav>
            <div class="top-margin"></div>
';
}
 ?>
<div class="wrapper">
	<div class="wholeBox">
		<p class="txt-center"><strong class="uppercase black">Tato služba je poskytována pouze v kombinaci s televizními službami nebo s připojením k internetu.</strong></p>
		
		<div class="smaller-wrapper">
			<p class="txt-center">Nabízíme kvalitní telefonní linku za výrazně nižší ceny než konkurence. Díky poskytování pouze na vlastní přístupové síti je zaručena vysoká kvalita a spolehlivost služby.</p>
		</div>
		
		<h2 class="blue uppercase txt-center mt25">Tarif pevné linky</h2>
		
		<table>
			<tr>
				<td></td>
				<td>Silný provoz (7-19h)</td>
				<td></td>
				<td>Slabý provoz (19-7h)</td>
				<td></td>
			</tr>
			<tr>
				<td>Místní a meziměstské volání</td>
				<td><strong>0.83,-</strong></td>
				<td></td>
				<td><strong>0.48,-</strong></td>
				<td></td>
			</tr>
			<tr>
				<td>Volání do mobilních sítí ČR</td>
				<td><strong>1.94,-</strong></td>
				<td></td>
				<td><strong>1.94,-</strong></td>
				<td></td>
			</tr>
			<tr>
				<td>Volání do sítí PODA</td>
				<td><strong>ZDARMA</strong></td>
				<td></td>
				<td><strong>ZDARMA</strong></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Tarifikace pro místní a meziměstské volání</td>
				<td></td>
				<td><strong>120+1</strong></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Tarifikace pro hovorné do domácích sítí</td>
				<td></td>
				<td><strong>60+1</strong></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Tarifikace pro hovorné do zahraničních sítí</td>
				<td></td>
				<td><strong>60+1</strong></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		
		<br class="clear" />
		
		<div class="sl30">
			<p>*v ceně měsíčního paušálu je zahrnut kredit 50 kč na hovorné. Přidělení volného kreditu 50 Kč se vztahuje na všechny klienty využívající internetové a TV služby PODA. Změna konfigurace IP telefonu (změna jména, rychlé volby, povolení přístup, aj.) je účtována 300Kč/úkon. Kompletní ceník VoIP včetně cen volání do zahraničí naleznete zde.</p>
		</div>
		
		<div class="price spanTop"><span>Měsíční paušál*</span> 30,-</div>
		
		<a href="/volani/chci-tarif/?tarif=pevná linka" class="button blue abs-bottom">Chci pevnou linku</a>
	</div>
	
	
	<div class="smaller-wrapper">
		<p class="txt-center">Volání je doplňková služba pro naše klienty! Zaujala Vás naše nabídka? <a href="/kontakty/" class="blue">Kontaktovat</a> nás můžete písemně nebo využijte zákaznickou linku</p>
		<p class="txt-center"><strong class="bigger blue">844 844 033</strong> nebo <strong class="bigger blue">730 430 430</strong></p>
	</div>
	
	<a href="/internet/" class="button huge red">Více o akčních balíčcích TV a internetu!</a>
	
	<br class="clear" />
</div>

<div class="blok cover_img more-text" data-src="/css/layout/banner/blokPevna1.jpg">
	<h2>Doplňkové služby a příslušenství k pevné lince</h2>
	
	<div class="wrap"> 
		<ul>
			<li>Možnost zachování stávajících telefonních čísel</li>
			<li>Zdarma on-line okamžitý podrobný výpis hovorů</li>
			<li>Služba CLIP - identifikace volajícího</li>
			<li>Automatické přesměrování, podmíněné přesměrování</li>
			<li>Přesměrování na více čísel</li>
			<li>Sériové linky</li>
			<li>Možnost předávání/přepojování hovorů *</li>
			<li>Více hovorů současně *</li>
			<li>Identifikace zmeškaných hovorů přímo na telefonu *</li>
		</ul>
		
		<p>* pouze s použitím IP telefonu</p>
	</div>
</div>

<div class="wrapper">
	<div class="mobileTarifs internet">

		<div class="mobileT">
			<div class="title">Fiber</div>
			
            <div class="row">
                <label>Ideální internet pro malou domácnost, která přivítá možnost připojení dalších zařízení.</label>
                <label>Aktivace internetu jednorázově 500,-</label>
                <a href="/tv-sluzby/podanet-tv/">2 PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">1 SIM karta PODA mobil</a>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>100/30<br/><span>Mbps</span></div>

		</div>
        <div class="mobileT">
            <div class="title">Rychlý Fiber</div>

            <div class="row">
                <label>Rychlý internet na doma i pro malé podnikání. K němu televize do kapsy.</label>
                <label>Aktivace internetu jednorázově 0,-</label>
                <a href="/tv-sluzby/podanet-tv/">4 PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">3 SIM karta PODA mobil</a>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>250/80<br/><span>Mbps</span></div>

        </div>
        <div class="mobileT">
            <div class="title">Senior Fiber</div>

            <div class="row">
                <label>Není senior jako senior. Někdo počítač nepotřebuje a jiný od něj nevstane. A pro vás máme speciální tarif.</label>
                <label>Aktivace internetu jednorázově 500,-</label>


            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>10/1<br/><span>Mbps</span></div>

        </div>

	</div>
	
	<br class="clear" />
	<br/>
    <h3>Volitelné varianty nastavení optického připojení k internetu</h3>
    <span>Rychlost (příchozí/odchozí)</span>
    <div class="table-responsive fst-table">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Typ</th>
                <th>Varianta 1</th>
                <th>Varianta 2</th>
                <th>Varianta 3</th>

            </tr></thead>
            <tr>
                <td>Fiber</td>
                <td>50/10 Mbps</td>
                <td>30/30 Mbps</td>
                <td>10/50 Mbps</td>
            </tr>
            <tr>
                <td>Rychlý Fiber/Fiber GPON</td>
                <td>100/30 Mbps</td>
                <td>65/65 Mbps</td>
                <td>30/100 Mbps</td>
            </tr>
            <tr>
                <td>Rychlý Fiber GPON</td>
                <td>250/80 Mbps</td>
                <td>165/165 Mbps</td>
                <td>80/250 Mbps</td>
            </tr>
        </table>
    </div>


	<br class="clear" />
</div>


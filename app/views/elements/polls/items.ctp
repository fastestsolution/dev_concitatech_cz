<?php if (isset($poll['Poll']['data']) && count($poll['Poll']['data'])>0){?>
<strong class="poll_name"><?php echo $poll['Poll']['name']?></strong>
		<ul>
		<?php 
		$total_count = 0;
		foreach ($poll['Poll']['data'] AS $data){ 
			list($odpoved,$hlasu) = explode('|',$data);
			$total_count+=$hlasu;
		}
		 
		?>
		<?php foreach($poll['Poll']['data'] AS $key=>$data){ ?>
			<?php list($odpoved,$hlasu) = explode('|',$data); ?>
			<li>
				
				<?php
					$procento = ($total_count/100);
					$width = $hlasu/$procento;
					
					echo '<span class="width none">'.round($width).'</span>';
					
					if ($poll['Poll']['type'] == 0)
						$count = round($hlasu/$procento).'%';
					else	
						$count = $hlasu.'x';
				?>
				<span>
					<?php echo $count; ?>
				</span>
				
				<?php 
				if (!isset($novote) && !isset($novote[$poll['Poll']['id']]))
					echo $html->link($odpoved,'/polls/'.$poll['Poll']['id'].'/'.$key,array('title'=>$odpoved),null,false); 
				else	
					echo $odpoved; 
				
				?>
				
				<var class="poll_pruh">&nbsp;</var>
				
				
			</li>
		<?php } ?>
		
		</ul>
		<?php echo lang_celkem_hlasu.': '.$total_count; ?>
<?php } ?>
<script type="text/javascript">
//<![CDATA[
var poll_id = $('<?php echo 'poll'.$poll_id;?>');
var poll_refresh = $('<?php echo 'poll_refresh'.$poll_id;?>');
polls(poll_id,poll_refresh);
//]]>
</script>
<br class="clear">
<?php 	
	echo '<div class="article_text print"><br class="clear">';
		echo '<h1>' . $page_caption . '</h1>';
		if (isset($article['Article']['text']) AND !empty($article['Article']['text']))
			echo $article['Article']['text']; 
		
		if (isset($article['Article']['fotogaleries']) && is_array($article['Article']['fotogaleries'])&& count($article['Article']['fotogaleries']) > 0){
			echo $this->renderElement('../articles/elements/fotogalerie',array('article'=>$article));
		}
		
		if (isset($article['Article']['addons']) && is_array($article['Article']['addons'])&& count($article['Article']['addons']) > 0){
			echo $this->renderElement('../articles/elements/addons',array('pozice'=>1));
		}
	echo '</div>';
?>

	

<div class="middleText">
<h1 class="title">Výsledky vyhledávání<small> #<?php echo $this->params['url']['data']['search_fulltext']; ?></small></h1>
</div>
</div>

<br class="clear" />

<div id='fst_history' class="wrapper mt50">
<?php 
		echo $this->renderElement('../articles/index_items');
?>
</div>

<?php if(strtolower($this->params['url']['data']['search_fulltext']) == 'platba') { ?>
<h2 class="title txt-center big mt50">FAQ</h2>
<div class="wrapper">
	<div class="faq" id="faq">
		<div class="fRow mt25">
		<div class="fRowTitle showMoreActual">Jakým způsobem budu dostávat faktury – Kde najdu vyúčtování <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>Faktury jsou zasílány elektronicky na e-mail, který je uveden ve smlouvě. Pokud máte sjednáno písemné zasílání faktur, pak faktury obdržíte poštou a elektronicky na Váš e-mail. Všechny faktury si můžete zobrazit prostřednictvím Klientské zóny (odkaz).</p>
		</div>
		<div class="fRow">
		<div class="fRowTitle showMoreActual">Jak si ověřím, že jste přijali mou platbu <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>Po připsání platby na Váš individuální účet PODA je vždy e-mailem zaslána automaticky generovaná zpráva o přijetí Vaší platby. Zasílání těchto zpráv je možné si nastavit prostřednictvím Klientské zóny. Všechny Vaše platby si můžete také kontrolovat prostřednictvím své <a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank" class="blue">Klientské zóny</a></p>
		</div>
		
		<div class="fRow">
		<div class="fRowTitle showMoreActual">Nestíhám zaplatit vyúčtování včas, co mám dělat <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>Na našich kontaktních místech může Vaše vyúčtování uhradit i Vámi pověřená osoba. Využijte možnost úhrady bankovním převodem. Důležité je uvést správný variabilní symbol, který naleznete na každé faktuře.</p>
		</div>
		
		<div class="fRow">
		<div class="fRowTitle showMoreActual">Co se stane, když nezaplatím včas <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>V případě prodlení s platbou Vás elektronicky upozorníme, že je evidována faktura po lhůtě splatnosti, kdy Vám bude stanovena i náhradní lhůta pro její úhradu.
Jestliže nebude ani v náhradním termínu vyúčtování uhrazeno, dojde k automatickému pozastavení (přerušení) poskytování služeb, až do doby zaplacení pohledávky. Po dobu takového pozastavení služeb jsou i nadále účtovány poplatky dle ceníku.</p>
		</div>
		
		<div class="fRow">
		<div class="fRowTitle showMoreActual">Blokujete mě kvůli neuhrazenému vyúčtování, co mám dělat <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>Co nejdříve uhradit všechny faktury, které jsou po lhůtě splatnosti. Dále viz možnosti úhrady faktur. Po připsání a spárování platby dojde k automatickému obnovení služeb.</p>
		</div>
		
		<div class="fRow">
		<div class="fRowTitle showMoreActual">Kde si mohu zkontrolovat nastavení a využívání služeb a jednotlivá vyúčtování <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>Plnou kontrolu nad svými údaji, službami, vyúčtováním i platbami máte prostřednictví <a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank" class="blue">Klientské zóny</a></p>
		</div>
		
		<div class="fRow bottomLine">
		<div class="fRowTitle showMoreActual">Jak účtujete aktivaci/zrušení tarifu nebo balíčku <a class="button circle showMoreActual" href="#">+</a></div>
		
		<p>Aktivační, deaktivační a jiné poplatky se řídí platným ceníkem PODA a.s. Poplatky související s rozšířením služeb jsou uvedeny u každého tarifu na našich webových stránkách. Poplatky za změnu tarifu v rámci poskytnutých služeb se neúčtují.</p>
		</div>
		
	</div>
	
	<br class="clear" />
	<a href="/kontakty/kontaktni-formular/" class="button wider">Pomoc, potřebuji více informací</a> 
	
</div>
<?php } ?>
<?php //pr($calendar_data); ?>
<div class="relative">
<div id="cal_preloader" class="none"></div>
<table>
<tr>
	<td>
		<table>
		<tr>		
			<td class="lft"><a class="month_link prev" href="<?php echo "/kalendar/". $prev_month . "/" . $prev_year; ?>" title="Předchozí měsíc" ></a></td>
		
			<td colspan="5" class="year"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
			<td class="rght"><a class="month_link next" href="<?php echo "/kalendar/". $next_month . "/" . $next_year; ?>" title="Další měsíc" ></a>  </td>
			
		</tr>
		<?php //pr($monthNames); ?>
		<tr class="days">
			<td>Po</td>
			<td>Út</td>
			<td>St</td>
			<td>Čt</td>
			<td>Pá</td>
			<td>So</td>
			<td>Ne</td>
		</tr>
		<?php
		$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
		$maxday = date("t",$timestamp);
		
		$thismonth = getdate ($timestamp);
		$startday = $thismonth['wday']-1;
		if ($startday < 0) $startday = 6;
		//pr($startday);
		//pr($maxday+$startday);
		for ($i=0; $i<($maxday+$startday+$startday); $i++) {
			if(($i % 7) == 0 ) echo "\n<tr>";
			
			if($i < $startday) echo "<td></td>";
			elseif($i > $maxday+$startday) echo "<td></td>";
			
			else {
			if ($i < $maxday+$startday){
			if ($thismonth['mon'] < 10) $month = '0'.$thismonth['mon'];
			else $month = $thismonth['mon'];
			
			$day = $i - $startday + 1;
			
			if ($day < 10) $day = '0'.$day;
			
			$date = $thismonth['year'].'-'.$month.'-'.$day;
			$today = date('Y-m-d');
			//pr($date);
			//pr($calendar_data);
			echo "\n<td class='day  ".((isset($calendar_data[$date]))?'active':'').' '.(($date == $today)?'today':'')."' data-date='".$date."'>";
				echo '<div class="relative">';
				
				if (isset($calendar_data[$date])){
					echo '<div class="point none" data-date="'.$date.'">'.$calendar_data[$date][0]['name'].'</div>';
					echo $html->link('','/koncerty/'.$date,array('title'=>'Koncerty'),null,false);
					
					
				
				
				
				}
				
				
				
				echo '<span class="date">'.($i - $startday + 1).'</span>' ;
				echo '</div>';
			echo "</td>";
			} else {
				echo '<td></td>';
			}
			}
			if(($i % 7) == 6 ) echo "</tr>\n";
			
		}
		//echo '</tr>';
		?>
		</table>


</td>
</tr>
</table>
</div>

<script type="text/javascript">
//<![CDATA[
calendar('<?php echo $calendar_id;?>');
//]]>
</script>
<br class="clear" />
<div class="wrapper">
    <h1 class="txt-center">Kontakty</h1>
    <p class="nonstop">
    NONSTOP LINKA
    </p>
    <div class="row">
        <div class="col-xs-12 col-sm-6 text-right">
            <a class="btn-conci btn-contact pull-right" href="mailto:info@conciatech.cz">info@conciatech.cz</a>
        </div>
        <div class="col-xs-12 col-sm-6">
            <a class="btn-conci btn-contact pull-left" href="tel:604-401-322">+420 604 401 322</a>
        </div>
    </div>
</div>
<div class="container mt50">
    <div class="row">
        <div class="col-xs-12 col-md-6 contacts">
            <h3 class="uppercase">Firemní údaje</h3>
            <address>
                <p>
                    <b>Concia tech s.r.o.</b><br/>
                    Národních Mučedníků 499<br />
                    738 01 Frýdek - Místek<br />
                    <br />
                    <b>IČ: 03323889</b><br />
                    <b>DIČ: CZ03323889</b><br />
                    <b>DIČ(SVK): SK4120006385</b><br />
                    <br /><br />
                    <div class="row">
                        <h3 class="uppercase">Kontaktní údaje</h3>
                        <div class="col-xs-12 col-md-6">
                            <p>
                                <b>Radek Přibyla</b><br />
                                Jednatel společnosti<br>
                                <a href="tel:604-865-549">+420 604 865 549</a><br/>
                                <a href="mailto:pribyla@conciatech.cz">pribyla@conciatech.cz</a>
                            </p>
                            <br>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <p>
                                <b>Marcela Hejlová</b><br />
                                Projektová manažerka<br />
                                <a href="tel:604-400-907">+420 604 400 907</a><br />
                                <a href="mailto:hejlova@conciatech.cz">hejlova@conciatech.cz</a><br /><br />
                            </p>
                        </div>
                    </div>
                   
                </p>
            </address>
            <br />
            <br />
           
        </div>
        <div class="col-xs-12 col-md-6">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2581.476157716171!2d18.361932316044506!3d49.68300545024326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4713e145a20cdde5%3A0xb35c09cf87a901c6!2sConcia+tech+s.r.o.!5e0!3m2!1scs!2scz!4v1522923203566" class="contact-map" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        
    </div>
    <!-- <h3 class="uppercase">Kontaktní údaje</h3> -->
            <div class="row contacts">
                <div class="col-md-4">
                    <p>
                        <b>Ing. Michal Vršecký</b><br />
                        Biologické poradenství a technická podpora<br />
                        <a href="tel:605-279-768">+420 605 279 768</a><br />
                        <a href="mailto:vrsecky@conciatech.cz">vrsecky@conciatech.cz</a><br /><br />
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <b>Marek Žídek</b><br />
                        Administrativa<br />
                        <a href="tel:737-052-127">Tel. + 420 737 052 127</a><br />
                        <a href="mailto:info@conciatech.cz">info@conciatech.cz</a><br /><br />
                    </p>
                </div>
            </div>
</div>
<?php //echo $this->renderElement('layout/footer_contact'); ?>
<div id="mainPage"></div>

<div class="wrapper mt50">
    <h2 data-aos="zoom-in">Jsme váš partner v oblasti servisu a oprav bioplynových stanic</h2>
    <p data-aos="zoom-in">
        Naše společnost se od roku 2014 zaměřuje zejména na poskytování služeb a dodávku zařízení v oblasti servisu a oprav technologie bioplynových stanic v ČR a zahraničí. Naším cílem je poskytovat služby na vysoké úrovní, a to lze docílit zejména odbornou kvalifikaci naších zaměstnanců spolu s profesionálním přístupem a pracovním vybavením, do kterého jsme investovali nemalé finanční prostředky, a to vše pouze ze zisku naší společnosti. Jsme ryze Česka společnost, která se poměrně vzestupně rozvijí, neovlivněná žádným jiným subjektem ani fyzickou osobou z venčí.
    </p>
    <br class="clear" >
    <div class="row row-h mt50">
        <div class="sll wrapper-img">
            <img class="img-responsive" alt="Conciatech"  data-aos="fade-left" src="/css/layout/photo/photo_homepage1.jpg" />
        </div>
        <div class="slr">
            <span class="highlight"   data-aos="fade-right" data-aos-offset="300">
                "Za celé období naší působnosti na trhu, jsme získali řadů spokojených zákazníků (viz. uvedené reference) ale také naších stálých dodavatelů."
            </span>
        </div>
    </div>
    <div class="row row-h mt50">
        <div class="sll">
            <p  data-aos="fade-left" data-aos-offset="300">
                Zaměřujeme se zejména na opravy a pravidelný servis bioplynových stanic, inovaci technologií, opravu a servis jednotlivých strojních součástí, zajišťujeme dodávku veškerých komponentů technologie v nejkratším možném termínů, dále dodávky a opravy plynových membrán různých velikosti, zajišťujeme veškerou projektovou dokumentaci. Nově se i zabýváme realizací rozvodů chladicích a tepelných soustav soustav ve velkých průmyslových podnicích, montáž ocelových konstrukcí a zajištujeme biologické a technologické poradenství pro provozovatele bioplynových stanic.
            </p>
        </div>
        <div class="slr wrapper-img">
            <img data-aos="fade-right" class="img-responsive" alt="Conciatech" src="/css/layout/photo/photo_homepage2.jpg" />
        </div>
    </div>
</div>
<br class="clear" >

<!--<div class="wrapper">

    <?php
    //echo '<h2>'.$smallbox_list[12]['Smallbox']['name'].'</h2>';
    //echo $smallbox_list[12]['Smallbox']['text'];
    ?>
</div>-->
<?php //echo $this->renderElement('layout/footer_contact'); ?>


<?php /*
<div class="sll">  
	<div class="article">
		<h3><a href="/novinka/<?php echo $actuals[0]['Actual']['alias_']; ?>/<?php echo $actuals[0]['Actual']['id']; ?>/"><?php echo $actuals[0]['Actual']['name']; ?></a></h3>
		
		<p><?php echo $actuals[0]['Actual']['perex']; ?></p>
	</div>
</div>

<div class="slr">	
	<div class="article">
		<h3><a href="/novinka/<?php echo $actuals[1]['Actual']['alias_']; ?>/<?php echo $actuals[1]['Actual']['id']; ?>/"><?php echo $actuals[1]['Actual']['name']; ?></a></h3>
		
		<p><?php echo $actuals[1]['Actual']['perex']; ?></p>
	</div>
</div>
*/ ?>
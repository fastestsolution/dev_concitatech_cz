<br class="clear" />

<div class="container">
	<div class="wrapper">
		<h1 class="txt-center">Zadat poptávku</h1>
	</div>
	
    <form method="post" id="poptavka" action="">
        <h3>Firemní údaje</h3>
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/firma',array('required'=>true,'placeholder'=>'Firma','class'=>'text input required')); ?>

            </div>
            <div class="col-xs-12 col-lg-3">
                <?php echo $htmlExt->input('ContactForm/ic',array('required'=>true,'placeholder'=>'IČ','class'=>'text input required')); ?>
            </div>
            <div class="col-xs-12 col-lg-3">
                <?php echo $htmlExt->input('ContactForm/dic',array('required'=>false,'placeholder'=>'DIČ','class'=>'text input')); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/kontaktni_osoba',array('required'=>true,'placeholder'=>'Kontaktní osoba','class'=>'text input required')); ?>
            </div>
        </div>
        <div class="row">
            <h3>Korespondenční údaje</h3>
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/ulice',array('required'=>true,'placeholder'=>'Ulice','class'=>'text input required')); ?>
            </div>
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/mesto',array('required'=>true,'placeholder'=>'Město','class'=>'text input required')); ?>
            </div>
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/psc',array('required'=>true,'placeholder'=>'PSČ','class'=>'text input required')); ?>
            </div>
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/zeme',array('required'=>true,'placeholder'=>'Země','class'=>'text input required')); ?>
            </div>
        </div>
        <div class="row">
            <h3>Kontaktní údaje</h3>
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/telefon',array('required'=>true,'placeholder'=>'Telefon','class'=>'text input required')); ?>
            </div>
            <div class="col-xs-12 col-lg-6">
                <?php echo $htmlExt->input('ContactForm/email',array('required'=>true,'placeholder'=>'Email','class'=>'text input required')); ?>
            </div>
        </div>
        <div class="row">
            <h3>Specifikace poptávky</h3>
            <div class="col-xs-12 col-lg-12">
                <?php echo $htmlExt->input('ContactForm/poptavana_sluzba',array('required'=>false,'placeholder'=>'Poptávaná služba','class'=>'text input')); ?>
            </div>
            <div class="col-xs-12 col-lg-12">
                <?php echo $htmlExt->textarea('ContactForm/poznamky',array('placeholder'=>'Poznámky','class'=>'text input')); ?>

            </div>
        </div>
        <?php echo $htmlExt->submit('Odeslat poptávku',array('id'=>'poptavkaSend','class'=>'btn-conci black button blue'));?>

    </form>
</div>

<br class="clear" />

<script>
	poptavka_form();
</script>

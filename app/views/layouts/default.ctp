<?php
if (isset($setting['name'])) $main_name = $setting['name'];
else $main_name = @$setting['name'];
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->renderElement('layout/html_head'); ?>
		<?php echo $this->renderElement('layout/js_css'); ?>
		<?php
			if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $javascript->link($link);}} else {echo $javascript->link($scripts);}}
			if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $html->css($style);} } else {echo $html->css($styles);}}
		?>
		<title>
			<?php echo $fastest->page_title(($this->params['url']['url']=='/')?array():(isset($fastlinks)?$fastlinks:''),$main_name);?>
		</title>
    </head>
    <body id="body" class="<?php echo (isset($on_main_page)?'main-page':'other-pages');  echo ((strpos($_SERVER['REQUEST_URI'], 'sluzby') !== false) ? ' sluzby' : '');?>">
        <!-- <div id="hide-blur-offset"></div> -->
        <div id="wrapper-preloader">
            <div id="preloader">
                <img src="/css/layout/icon/bars.svg" class="loader-icon" width="90" alt="">
            </div>
        </div>

        
        <?php if(isset($on_main_page)) { ?>
			<section>
		        <div class="wrapper" id="firstWrapper">
		            <h1><?php echo $page_caption; ?></h1>
                    <?php echo $this->renderElement('layout/seo_txt'); ?>
                    <?php echo $content_for_layout; ?>
		            
		            <?php echo $this->renderElement('layout/header'); ?>
		        </div>
	        </section>  
	    <?php } else { ?>   
		    <section class="other">
                <div id="firstWrapper">
				<?php echo isset($fastlinks) ? '<h1 class="seo_title">'.key($fastlinks).' Concia tech s.r.o.</h1>' :'<h1 class="seo_title">Concia tech s.r.o.</h1>'; ?>
                 <?php echo $this->renderElement('layout/seo_txt'); ?>
				<?php echo $content_for_layout; ?>
                </div>
		    </section>
		    
		    <?php echo $this->renderElement('layout/header'); ?>
        <?php } ?>

        <header class="header <?= (!isset($on_main_page) ? 'smaller' : '' ); ?>" id="header_adopt">
                <div id="banner-2">
                    <div id="arrow2-left"> </div>
                    <div id="arrow2-right"> </div>
                    <div id="arrow2-bottom"> </div>

                    <div class="pagination-imgs" id="banner-2_pagination">
                        <a class="point"> </a>
                        <a class="point"> </a>
                        <a class="point"> </a>
                    </div>
                    <div class="img own-img" data-src="/css/layout/banner/slide1.jpg"></div>
                    <div class="img own-img" data-src="/css/layout/banner/slide2.jpg"></div>
                    <div class="img own-img" data-src="/css/layout/banner/slide3.jpg"></div>
                    <div class="img own-img" data-src="/css/layout/banner/slide4.jpg"></div>
                    <div class="img own-img" data-src="/css/layout/banner/slide5.jpg"></div>
                </div>
                <?php if(isset($on_main_page)) { ?>
                    <div class="header_uvod" data-aos="zoom-in">
                        <span>Servis a opravy bioplynových stanic.</span>
                        <i class="scrollMobile awesome"> &#xf103;</i>
                        <?php // <a class="btn-conci" href="/zadat-poptavku">Zadat poptávku</a> ?>
                    </div>
                <?php } ?>
           
        </header>



        <!-- <div id="header_adopt" data-src="/css/layout/banner/banner_homepage.jpg" class="<?php //echo (isset($on_main_page)?'banner':''); ?> cover_img">
            
        </div> -->
        <br class="clear">
        <div class="wrapper mt50">
            <?php echo $this->renderElement('layout/footer_contact'); ?>

        </div>

        <footer>
                <?php echo $this->renderElement('layout/footer'); ?>
               
                <?php echo $this->renderElement('layout/footer_statistiky'); ?>
                <?php echo $this->renderElement('layout/footer_copyright'); ?>
            </div>
        </footer>
        
	<?php echo $this->element('sql_dump'); ?>
     </body>
</html>
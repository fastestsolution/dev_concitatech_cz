<div class="contact_form noprint">
<form class="form" action='/contact_forms/<?php echo $form_id?>/' method="post" onsubmit='return false;' id="ContactForm<?php echo $form_id?>">
	<?php echo $htmlExt->hidden('ContactForm/spam',array());?>
	<?php echo $htmlExt->input('ContactForm/name',array('tabindex'=>999,'class'=>'text input validate[\'required\',\'email\']','tabindex'=>1000,'placeholder'=>lang_email));?>
	<?php //pr($form_data['ContactFormGroup']['data']); ?>
	<?php
	/*
	1 => 'Textové pole',
  	2 => 'Textová oblast',
  	3 => 'Výběrový seznam',
  	4 => 'Zatrhávací pole',
	*/
	if (isset($form_data['ContactFormGroup']['data'])){ 
		foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
			list($label,$type,$required,$data) = explode('|',$item);
			$find_const = strpos($label,'lang_');
			if ($find_const !== false)
				$label = constant($label);
			
			switch($type){
				case 1: // input
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text input '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 2: // textarea
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text input '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 3: // select
					$data_list_tmp = explode(';',$data);
					$data_list = array();
					foreach ($data_list_tmp AS $d)
						$data_list[$d] = $d;
					
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 4: // checkbox
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
			}
		}
	}
	?>
	
	<label>&nbsp;</label><?php echo $htmlExt->submit(lang_odeslat,array('id'=>'ContactForm'.$form_id.'_send','class'=>'button')).'<br />';?>	
</form>
</div>

<script type="text/javascript" language="javascript">
//<![CDATA[
	var contact_form_id = 'ContactForm<?php echo $form_id?>';
	$('ContactFormSpam').value = 123;
	contact_form(contact_form_id);
//]]>
</script>
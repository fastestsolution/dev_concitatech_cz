<?php
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 15.10.2009
 */
class FindsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Finds';
	var $uses = array('Article');
	var $layout = 'default';
	var $find = array(
		'Article' => array(
			'caption' 	=> 'Články',
			'url'		=> '/clanek/{Article.alias_}/{Article.id}',
			'cols' 		=> array(
				'Fname',
				'Fabstrakt',
				'Ftext',
				'alias_',
				'id'
			),
		),
		'Actual' => array(
			'caption' 	=> 'Aktuality',
			'url'		=> '/aktualita/{Actual.alias_}/{Actual.id}',
			'cols' 		=> array(
				'Fname',
				'Ftext',
				'alias_',
				'id'
			),
		)
	);
	
	var $page_limit = 10;
		
	function index($find_something=''){
		$this->set('page_caption', lang_search_vyhledavani.' - '.$find_something);	
		$this->set('fastlinks',array(lang_search_vyhledavani.' - '.$find_something => '#'));	
		$results = array();
		foreach($this->find as $modelClass=>$type){
			$model = ClassRegistry::init($modelClass, 'Model');
			
			$condition 	= isset($type['conditions'])?$type['conditions']:array();
			$fields 	= array();
			
			foreach($type['cols'] as $field){
				if (substr($field,0,1) == 'F'){
					$field = substr($field,1);
					$condition['OR']["$modelClass.$field LIKE "] = "%$find_something%";
				}
				$fields[] =   "$modelClass.$field";
				
			}
			
			$results[$modelClass] = array( 
				'find' => $model->find(
								'all',
								array(
									'conditions'=>$condition,
									'fields' => $fields
								)
							),
				'caption' 	=>  $type['caption'],
				'url'		=> 	$type['url']
			);
			unset($condition, $model);
		}
		$this->set('results',$results);
	}
	
	
	
}	
?>
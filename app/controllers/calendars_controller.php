<?php
class CalendarsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Calendars';
	var $uses = array('Calendar');
	
	
	function load($date){
		$conditions = array(
			'Calendar.status'=>1,
			'Calendar.kos'=>0,
			'Calendar.date'=>$date,
		);
		$fields = array(
			'Calendar.id',
			'Calendar.name',
			'Calendar.text',
			'Calendar.date',
		);
		
		$load = $this->Calendar->find('all',array('conditions'=>$conditions,'fields'=>$fields));
		if (!$load)
			die(json_encode(array('resutl'=>false,'message'=>'Chyba načtení dat')));
		else	
			die(json_encode(array('resutl'=>true,'data'=>$load)));
		
	}
	
	function index($month=null,$year=null,$calendar_id=null){
		if ($calendar_id != null)
		$this->set('calendar_id',$calendar_id);
		
		$conditions = array(
			'Calendar.status'=>1,
			'Calendar.kos'=>0,
		);
		$fields = array(
			'Calendar.id',
			'Calendar.name',
			'Calendar.text',
			'Calendar.date',
		);
		$load_all = $this->Calendar->find('all',array('conditions'=>$conditions,'fields'=>$fields));
		$calendar_data = array();
		foreach($load_all AS $c){
			$calendar_data[$c['Calendar']['date']][] = $c['Calendar'];
		}
		$this->set('calendar_data',$calendar_data);
		//pr($calendar_data);
		
		
		$this->set('monthNames',$monthNames = Array("Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec","Srpen", "Září", "Říjen", "Listopad", "Prosinec"));
		if ($month == null) $month = date("n");
		if ($year == null) $year = date("Y");
		
		$cMonth = $month;
		$cYear = $year;
		
		
		$prev_year = $cYear;
		$next_year = $cYear;
		$prev_month = $cMonth-1;
		$next_month = $cMonth+1;
		 
		if ($prev_month == 0 ) {
			$prev_month = 12;
			$prev_year = $cYear - 1;
		}
		if ($next_month == 13 ) {
			$next_month = 1;
			$next_year = $cYear + 1;
		}
		
		$this->set('prev_month',$prev_month);
		$this->set('next_month',$next_month);
		$this->set('prev_year',$prev_year);
		$this->set('next_year',$next_year);
		$this->set('cMonth',$cMonth);
		$this->set('cYear',$cYear);
			
		$this->set('page_caption','Kalendar');
		$this->set('fastlinks',array('Kalendar'=>'#'));	
		
		if ($this->RequestHandler->isAjax()){
			$this->render('calendar_item');	
		} else{
			$this->render('calendar');
		}
	}
	
	
}	
?>
<?php
	@define('actuals_link','aktuality');
	@define('actuals_link_detail','aktualita');
	@define('fotogalerie_link','fotogalerie');
	@define('guestbook_link','seznamka');
	
	Router::connect('/clear_cache/', array('controller' => 'pages','action'=>'clear_cache'));
	
	Router::connect('/'.guestbook_link.'/', array('controller' => 'guestbooks','action'=>'index',1));
	Router::connect('/pages/isUnique/*', array('controller' => 'pages','action'=>'isUnique'));
	Router::connect('/search/*', array('controller' => 'finds','action'=>'index'));
	Router::connect('/searchService/*', array('controller' => 'pages','action'=>'searchService'));
	Router::connect('/searchAddressSave/*', array('controller' => 'pages','action'=>'saveAddressSession'));
	Router::connect('/ispmail/*', array('controller' => 'pages','action'=>'isp_mail'));
	Router::connect('/test/*', array('controller' => 'pages','action'=>'test'));
	
	Router::connect('/kalendar/*', array('controller' => 'calendars','action'=>'index'));
	Router::connect('/kalendar-load/*', array('controller' => 'calendars','action'=>'load'));
	
	Router::connect('/fst_upload/*', array('controller' => 'pages','action'=>'fst_upload'));
	
	# START clanky
	Router::connect('/clanek/:alias/:id',	array('controller' => 'articles',	'action' =>	'detail'),array('pass' => array('alias','id'),'id' => '[0-9]+'));
	Router::connect('/clanek-fotogalerie/:id',	array('controller' => 'articles',	'action' =>	'article_fotogalerie'),array('pass' => array('id'),'id' => '[0-9]+'));
	
	Router::connect('/hledej/*',	array('controller' => 'articles',	'action' =>	'view'));
	
	Router::connect('/actuals_box/:id',	array('controller' => 'actuals', 'action'=>'actuals_box'),array('pass' => array('id'),'id' => '[0-9]+'));
	Router::connect('/'.actuals_link.'/:year/*',	array('controller' => 'actuals', 'action'=>'index'),array('pass' => array('year')));
	Router::connect('/'.actuals_link_detail.'/:alias/:id',	array('controller' => 'actuals', 'action'=>'detail'),array('pass' => array('alias','id'),'id' => '[0-9]+'));

	Router::connect('/'.fotogalerie_link.'/',	array('controller' => 'fotogaleries', 'action'=>'index'));
	Router::connect('/'.fotogalerie_link.'/*',	array('controller' => 'fotogaleries', 'action'=>'detail'));
	Router::connect('/google_maps/*',	array('controller' => 'google_maps', 'action'=>'index'));
	Router::connect('/polls/*',	array('controller' => 'polls', 'action'=>'index'));
	//Router::connect('/polls-vote/*',	array('controller' => 'polls', 'action'=>'index'));
	
	//specialni linky
	Router::connect('/google_sitemap/',	array('controller' => 'export_xmls',	'action' =>	'google_sitemap'));//cz
Router::connect('/', 					array('controller' => 'pages', 	'action' => 'homepage'));
	Router::connect('/mapa-stranek/',	array('controller' => 'articles',	'action' =>	'sitemap'));//cz
	Router::connect('/rss-aktuality/*',	array('controller' => 'actuals',	'action' =>	'rss'));//cz
	
	# contact forms
	Router::connect('/contact_forms/*', array('controller' => 'contact_forms','action'=>'index'));
	Router::connect('/sendPoptavka/', array('controller' => 'contact_forms','action'=>'sendPoptavka'));

	Router::connect('/smallbox/:id', array('controller' => 'pages','action'=>'smallbox'), array('pass' => array('id'),'id' => '[0-9]+'));
	Router::connect('/loadContactForm/*', array('controller' => 'pages', 'action' => 'loadContactForm'));
	
	Router::connect('/domu/', 					array('controller' => 'pages', 	'action' => 'homepage'));

	Router::connect('/reference/*', 			array('controller' => 'pages', 	'action' => 'reference'));
	Router::connect('/zadat-poptavku/*', 		array('controller' => 'pages', 	'action' => 'zadat_poptavku'));
	Router::connect('/kontakty/', 			array('controller' => 'pages', 	'action' => 'kontakty'));

	Router::connect('/examples/*',			array('controller' => 'pages', 	'action' => 'examples'));
	Router::connect('/*', 					array('controller' => 'articles', 	'action' => 'view'));
    //Router::connect('/', 					array('controller' => 'pages', 	'action' => 'homepage'));

?>
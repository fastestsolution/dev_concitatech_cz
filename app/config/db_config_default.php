<?php
/**
 * vytvori soubor db_config.php dle ENV promenych z pipeline, moznost vytvorit rucne s timto PHP
 * 
 * <?php
 *   return [
 *       'SQL_HOST'=>'localhost',
 *       'SQL_USERNAME'=>'root',
 *       'SQL_PASSWORD'=>'',
 *       'SQL_DATABASE'=>'pospiech'
 *   ];
 */


$sql = [
    'SQL_HOST'=>'{{SQL_HOST}}',
    'SQL_USERNAME'=>'{{SQL_USERNAME}}',
    'SQL_PASSWORD'=>'{{SQL_PASSWORD}}',
    'SQL_DATABASE'=>'{{SQL_DATABASE}}'
];
$pathConfig = "app/config/db_config.php";

// DEV prostredi
if ($_ENV['CONFIG_ENV'] == 'DEV'){
    if (isset($_ENV['SQL_HOST_DEV'])){
        $sql['SQL_HOST'] = $_ENV['SQL_HOST_DEV'];
    }
    if (isset($_ENV['SQL_NAME_DEV'])){
        $sql['SQL_USERNAME'] = $_ENV['SQL_NAME_DEV'];
    }
    if (isset($_ENV['SQL_PASSWORD_DEV'])){
        $sql['SQL_PASSWORD'] = $_ENV['SQL_PASSWORD_DEV'];
    }
    if (isset($_ENV['SQL_DB_DEV'])){
        $sql['SQL_DATABASE'] = $_ENV['SQL_DB_DEV'];
    }    
} 

// PROD prostredi
if ($_ENV['CONFIG_ENV'] == 'PROD'){
    if (isset($_ENV['SQL_HOST_PROD'])){
        $sql['SQL_HOST'] = $_ENV['SQL_HOST_PROD'];
    }
    if (isset($_ENV['SQL_NAME_DEV'])){
        $sql['SQL_USERNAME'] = $_ENV['SQL_NAME_PROD'];
    }
    if (isset($_ENV['SQL_PASSWORD_DEV'])){
        $sql['SQL_PASSWORD'] = $_ENV['SQL_PASSWORD_PROD'];
    }
    if (isset($_ENV['SQL_DB_DEV'])){
        $sql['SQL_DATABASE'] = $_ENV['SQL_DB_PROD'];
    }    
}

if (in_array($_ENV['CONFIG_ENV'], ['PROD','DEV'])){
        
    $fileData = "<?php return ".var_export($sql, true)."; ?>";
    print_r($fileData);

    $myfile = fopen($pathConfig, "w") or die("Unable to open file ".$pathConfig."!");
    fwrite($myfile, $fileData);
    echo 'Soubor vytvoren: '.$myfile;
} else {
    echo '!!!!!!! Neni definovano prostredi pro BUILD !!!!!!!!';
}
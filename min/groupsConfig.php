<?php
@define('MIN_ROOT_PATH','/');
@define('MIN_WEBROOT','/webroot');

// JS
$internal_js = array(
    '//js/mootools1.4/core1.6.js',   
    '//js/mootools1.4/c_more1.6.js',   
    '//js/history/fst_history.js', 
    // '//js/formcheck.js',   
    '//js/fstBox/fstBox.js',   // Fst Box
    '//js/pages.js',   //obdoba c_page_load.js
    '//js/less.js', 
    //'//js/jquery_script.js',   // bootstrap
    '//js/global_fce.js',   // globalni funkce pro JS


);
$external_js = array(
	// 'https://scripts.fastesthost.cz/js/mootools1.4/core1.6.js',
	// 'https://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js',
	// 'https://scripts.fastesthost.cz/js/history/fst_history.js',
	//'https://scripts.fastesthost.cz/js/formcheck/formcheck.js'
);
// CSS
$internal_css = array(
	'//css/css_compile/default.css',
    '//css/css_compile/response.css',
    '//css/bootstrap3-grid.css',

);
$internal_css_debug = array(
	'/css/default.less',
    '/css/response.less',
    '/css/bootstrap3-grid.css',

);
$external_css = array(
    'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
    //'https://scripts.fastesthost.cz/js/formcheck/theme/red/formcheck.css'
    //'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
);

/* neupravovat dale*/

$ext_css = array();
$options = stream_context_create(array('http'=>
    array(
        'timeout' => 5
    )
));
foreach($external_css AS $k=>$path){
    if(file_exists($path)){
        $content = @file_get_contents($path, false, $options);
        //$src = new Minify_Source(array('id' => 'source'.$k,'content' => $content,'contentType' => Minify::TYPE_CSS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));
        if($content){
            $src = new Minify_Source(array('id' => 'source'.$k,'content' => $content,'contentType' => Minify::TYPE_CSS,'lastModified' => 604800,));
            $ext_css[] = $src;
        }
    }


	

}

$ext_js = array();
foreach($external_js AS $k=>$path){
    if(file_exists($path)){
        $content = file_get_contents($path);
        $src = new Minify_Source(array('id' => 'source'.$k,'content' => $content,'contentType' => Minify::TYPE_JS,'lastModified' => 604800,));
        $ext_js[] = $src;
    }

}

$result = array(
	'css'=>array(),
	'js'=>array(),
);
$result['css'] = array_merge($ext_css,$internal_css);

$result['js'] = array_merge($ext_js,$internal_js);

foreach($internal_js AS $k=>$i){
	$internal_js[$k] = '/'.ltrim($i,'/');
}
foreach($internal_css AS $k=>$i){
	$internal_css[$k] = '/'.ltrim($i,'/');
}

$result['js_links'] = array_merge($external_js,$internal_js);
$result['css_links'] = array_merge($external_css,$internal_css_debug);
//pr($result['js_mobile']);die();

    
return $result;
<?php
$config = [
	'my site' => [
		// 'remote' => 'ftp://user:secretpassword@ftp.example.com/directory',
		// 'remote' => 'sftp://109.123.216.45tst',
		'remote' => $_ENV['FTP_HOST_DEV'].$_ENV['FTP_PATH_DEV'],
		'local' => '../',
		'test' => false,
		'ignore' => '
			/deployment.*
			app/tmp/logs/*
			app/tmp/cache/models/*
			app/tmp/cache/persistent/*
			app/tmp/cache/view/*
			/cake/*
			/vendor/*
			/production/*
			/rollbar/*
			/node_modules
			/.git
			!temp/.htaccess
			*/tests
			/uploaded
			/zaloha
			/css/*.less
			/new
			composer.json
			composer.lock
			bitbucket-pipelines.yml
			test.html
        ',
        'user'=> $_ENV['FTP_USER_DEV'],
        'password'=> $_ENV['FTP_PASSWORD_DEV'],

		// 'include' => '
        // 	/app
        // 	/app/*
        // 	/index.php
        // ',

		'allowDelete' => true,
		'before' => [
			function (Deployment\Server $server, Deployment\Logger $logger, Deployment\Deployer $deployer) {
				$logger->log('Spusteni deploing na DEV server!');
			},
		],
		'afterUpload' => [
			// 'http://example.com/deployment.php?afterUpload'
		],
		'after' => [
            'remote: chmod 0777 app/tmp/logs',
            'remote: chmod 0777 app/tmp/cache',
            'remote: chmod 0777 app/tmp/cache/models',
            'remote: chmod 0777 app/tmp/cache/persistent',
            'remote: chmod 0777 app/tmp/',
            'remote: chmod 0777 css/css_compile/',
            'remote: chmod 0777 css/css_log.log',
            'remote: chmod 0777 css/css_compile/default.css',
            'remote: chmod 0777 css/css_compile/response.css',
            'remote: chmod 0777 css/',
			'remote: chmod 0777 cake',
			'remote: chmod 0777 production',
        	'http://conciatech2.fastestdev.cz/unzip.php'
		    // 'upload: cake/webroot/configPokladnaDev.json cake/webroot/configPokladna.json',
			// 'http://example.com/deployment.php?after'
		],
		'purge' => [
			// 'tmp/cache/persistent',
			// 'tmp/cache/models',
		],
		// 'preprocess' => ['combined.js', 'combined.css'],
	],

	'tempDir' => __DIR__ . '/temp',
	'colors' => true,
];
// print_r($_ENV);die();
//  print_r($config); 
//  die();
return $config;